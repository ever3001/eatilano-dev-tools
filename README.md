# Personal Development Tools

This repository provides a collection of small tools, confs, scripts that I have use when developing projects.

## Tools

### Linux

- Oh My Zsh is an open source, community-driven framework for managing your zsh configuration: https://ohmyz.sh/
- A simple, fast and user-friendly alternative to "find": https://github.com/sharkdp/fd
- General-purpose command-line fuzzy finder: https://github.com/junegunn/fzf
- Cool terminal emulator like xterm, gnome-terminal, konsole, etc. https://terminator-gtk3.readthedocs.io/en/latest/
- ripgrep is a line-oriented search tool that recursively searches the current directory for a regex pattern. https://github.com/BurntSushi/ripgrep

### Embedded Linux

- Open source over the air softwtare updates for Linux devices: https://mender.io/

### Embedded C

- Transform C code into different assemblers code: https://godbolt.org/
- Unit test and mocking for embedded systems: https://www.throwtheswitch.org/

## Powerfull websites

- Code documentation for big projects like Linux, U-boot, Busybox, etc. https://elixir.bootlin.com/
- Library to find any book: https://z-lib.org/
- Usefull bash commands: https://onceupon.github.io/Bash-Oneliner/
- Advanced GDB Usage: https://interrupt.memfault.com/blog/advanced-gdb
- ShellCheck: https://github.com/koalaman/shellcheck

## Guides

### Embedded C

- How to implement Unions and structs for embedded C: https://atadiat.com/en/e-embedded-c-struct-union-part-2/

## VSCODE Plugins

### General

- TODO Highlight
- Todo Tree
- Better Comments
- Bracket Pair Colorizer 2
- Code Spell Checker
- Github Copilot
- Settings Sync

### Binaries

- Hex Editor
- Hex Editor with Tags

### Markdown

- Markdown All in One
- Markdown Preview Mermaid Support
- markdownlint
- Markdown PDF

### Git

- Git Lens
- Git Graph

### Embedded Systems

- PlatformIO

## Git configuration

```bash
git config --global user.name "FIRST_NAME LAST_NAME"
git config --global user.email "MY_NAME@example.com"
```
